##########################
### TERRAFORM SPECIFIC ###
##########################
terraform {
  required_version = ">= 0.12, < 0.13"
}




################
### PROVIDER ###
################
provider "aws" {
  region = var.aws_region

  # allow any 2.x version of AWS provider
  version = "~> 2.0"
}




######################
### AWS S3 BUCKETS ###
######################
resource "aws_s3_bucket" "terraform_state" {
  bucket = var.tfs_s3_bucket_name

  # Prevent accidental deletion of S3 bucket
  lifecycle {
    prevent_destroy = true
  }

  # Enable versioning since we want to see full revision history of state files
  versioning {
    enabled = true
  }

  # Enable server-side encryption by default
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = var.tfs_s3_sse_algorithm
      }
    }
  }
}




#################
### DYNAMODB ###
#################
resource "aws_dynamodb_table" "terraform_locks" {
  name = var.tfs_dynamodb_name
  billing_mode = "PAY_PER_REQUEST"
  hash_key = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }
}
