####################
### AWS SPECIFIC ###
####################
variable "aws_region" {
  description = "AWS region"
  type = string
  default = "us-east-2"
}




############################
### S3 RELATED VARIABLES ###
############################
variable "tfs_s3_bucket_name" {
  description = "Terraform state S3 bucket name"
  type = string
  default = "terraform-state-87hg8123"
}

variable "tfs_s3_sse_algorithm" {
  description = "Terraform state S3 SSE algorithm"
  type = string
  default = "AES256"
}




##################################
### DYNAMODB RELATED VARIABLES ###
##################################
variable "tfs_dynamodb_name" {
  description = "Terraform state dynamodb name"
  type = string
  default = "terraform-locks-87hg8123"
}




###########################################
### TERRAFORM BACKEND RELATED VARIABLES ###
###########################################
variable "tfs_key_location" {
  description = "Terraform backed key location"
  type = string
  default = "global/s3/terraform.tfstate"
}
