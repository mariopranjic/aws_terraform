# Creating objects in AWS to support global shared terraform state

Based on Terraform up&running book.
In this example we deploy S3 bucket and DynamoDB table to support terraform state

## Quick start


Configure your [AWS access 
keys](http://docs.aws.amazon.com/general/latest/gr/aws-sec-cred-types.html#access-keys-and-secret-access-keys) as 
environment variables:

```
export AWS_ACCESS_KEY_ID=(your access key id)
export AWS_SECRET_ACCESS_KEY=(your secret access key)
```

Deploy the code:


Please note we have chicken and the agg situation here. in order to create s3 bucket we need to use local terraform state.
only when S3 and DynamoDB are in place, we can switch to using it for terraform state as noted here.

```
terraform init -backend-config=../backend.hcl
terraform apply
```


