# backend.hcl

# Settings are passed to terraform with:
#     terraform init -backend-config=backend.hcl

# NOTE: Variables here are not allowed and thus we have to copy values manually.
#       Not very practical, nor clean.

bucket = "terraform-state-87hg8123"
region = "us-east-2"
dynamodb_table = "terraform-locks-87hg8123"
encrypt = true
