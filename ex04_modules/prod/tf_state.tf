##################################
### TERRAFORM BACKEND SPECIFIC ###
##################################

# NOTE: Variables here are not allowed and thus we have to copy values manually.
#       Not very practical, nor clean.

# Partial configuration. The other settings (e.g., bucket, region) will be
# passed in from a file via -backend-config arguments to 'terraform init'
#
#       terraform init -backend-config=../backend.hcl
#

terraform {
  backend "s3" {
    key = "global/s3/ex04/prod/terraform.tfstate"
  }
}
