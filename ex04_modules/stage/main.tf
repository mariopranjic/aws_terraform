##########################
### TERRAFORM SPECIFIC ###
##########################
terraform {
  required_version = ">= 0.12, < 0.13"
}


################
### PROVIDER ###
################
provider "aws" {
  region = var.aws_region

  # allow any 2.x version of AWS provider
  version = "~> 2.0"
}




#############################
### Call webserver module ###
#############################
module "webserver_cluster" {
  source = "../../modules/services/webserver-cluster"

  ### MODULE RELATED VARIABLES ###
  # AWS SPECIFIC
  ec2_image = "ami-0c55b159cbfafe1f0"
  ec2_type = "t2.micro"

  # INSTANCE RELATED VARIABLES
  server_port = 8080
  instance_security_group_name = "stage-web-inst-sg"

  # NETWORK RELATED VARIABLES
  network_protocol = "tcp"
  network_cidr = ["0.0.0.0/0"]

  # SERVICE RELATED VARIABLES
  service_protocol = "HTTP"
  service_port = 80

  # LOAD BALANCER RELATED VARIABLES
  alb_name = "stage-web-lb"
  alb_security_group_name = "stage-lb-sg"

  # LB HEALTHCHECK RELATED VARIABLES
  hc_path = "/"
  hc_matcher = "200"
  hc_interval = 15
  hc_timeout = 3
  hc_treshold_h = 2
  hc_treshold_u = 3

  # AUTOSCALING GROUP RELATED VARIABLES
  asg_min_size = 2
  asg_max_size = 5
  webserver_name = "stage-web-asg"
  ### *** ###
}
