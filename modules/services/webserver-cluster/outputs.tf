###################
### LB DNS NAME ###
###################
output "alb_dns_name" {
  value = aws_lb.webserver-lb.dns_name
  description = "Domain name of LB"
}
