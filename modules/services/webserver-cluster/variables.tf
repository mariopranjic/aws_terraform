####################
### AWS SPECIFIC ###
####################
variable "ec2_image" {
  description = "AWS EC2 image"
  type = string
#  default = "ami-0c55b159cbfafe1f0"
}

variable "ec2_type" {
  description = "AWS EC2 instace type"
  type = string
#  default = "t2.micro"
}




##################################
### INSTANCE RELATED VARIABLES ###
##################################
variable "server_port" {
  description = "Port on which server opens ofr HTTP requests"
  type =  number
#  default = 8080
}

variable "instance_security_group_name" {
  description = "Security group name for EC2 instances"
  type = string
#  default = "webserver-instance-sg"
}




#################################
### NETWORK RELATED VARIABLES ###
#################################
variable "network_protocol" {
  description = "Network protocol"
  type = string
#  default = "tcp"
}

variable "network_cidr" {
  description = "List of CIDR blocks"
  type    = list(string)
#  default = ["0.0.0.0/0"]
}




#################################
### SERVICE RELATED VARIABLES ###
#################################
variable "service_protocol" {
  description = "Load balanced service protocol"
  type = string
#  default = "HTTP"
}

variable "service_port" {
  description = "Load balanced service port"
  type = number
#  default = 80
}




#######################################
### LOAD BALANCER RELATED VARIABLES ###
#######################################
variable "alb_name" {
  description = "Name of the ALB"
  type = string
  default = "webserver-lb"
}

variable "alb_security_group_name" {
  description = "Security group name for ALB"
  type = string
  default = "lb-sg"
}




########################################
### LB HEALTHCHECK RELATED VARIABLES ###
########################################
variable "hc_path" {
  description = "LB health check path"
  type = string
  default = "/"
}

variable "hc_matcher" {
  description = "LB health check matcher string"
  type = string
  default = "200"
}

variable "hc_interval" {
  description = "LB health check interval"
  type = number
  default = 15
}

variable "hc_timeout" {
  description = "LB health check timeout"
  type = number
  default = 3
}

variable "hc_treshold_h" {
  description = "LB health check healthy threshold"
  type = number
  default = 2
}

variable "hc_treshold_u" {
  description = "LB health check unhealthy threshold"
  type = number
  default = 3
}




###########################################
### AUTOSCALING GROUP RELATED VARIABLES ###
###########################################
variable "asg_min_size" {
  description = "Minimal number of ASG instances"
  type =  number
  default = 2
}

variable "asg_max_size" {
  description = "Maximal number of ASG instances"
  type =  number
  default = 5
}


###########################################
### WEBSERVER EC2 INSTANCE NAMES        ###
###########################################
variable "webserver_name" {
  description = "Webserver name tag"
  type = string
  default = "webservers"
}
