##########################
### TERRAFORM SPECIFIC ###
##########################
terraform {
  required_version = ">= 0.12, < 0.13"
}


################
### PROVIDER ###
################
provider "aws" {
  region = var.aws_region

  # allow any 2.x version of AWS provider
  version = "~> 2.0"
}




############
### DATA ###
############
data "aws_vpc" "default" {
  default = true
}

data "aws_subnet_ids" "default" {
  vpc_id = data.aws_vpc.default.id
}




#######################
### SECURITY GROUPS ###
#######################
resource "aws_security_group" "instance" {
  name = var.instance_security_group_name

  ingress {
    from_port = var.server_port
    to_port = var.server_port
    protocol = var.network_protocol
    cidr_blocks = var.network_cidr
  }
}


resource "aws_security_group" "alb" {
  name = var.alb_security_group_name

  # allow inbound HTTP requests
  ingress {
    from_port = var.service_port
    to_port = var.service_port
    protocol = var.network_protocol
    cidr_blocks = var.network_cidr
  }

  # allow all outbound requests
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = var.network_cidr
  }
}




############################
### LAUNCH CONFIGURATION ###
############################
resource "aws_launch_configuration" "webserver-conf" {
  image_id = var.ec2_image
  instance_type = var.ec2_type
  security_groups = [aws_security_group.instance.id]

  user_data = <<-EOF
              #!/bin/bash
              echo "Mario says HI" > index.html
              nohup busybox httpd -f -p ${var.server_port} &
              EOF

  # we wanna create new instances before removing existing ones
  lifecycle {
    create_before_destroy = true
  }
}




########################
### LB TARGET GROUPS ###
########################
resource "aws_lb_target_group" "asg" {
  name = var.alb_name
  port = var.server_port
  protocol = var.service_protocol
  vpc_id = data.aws_vpc.default.id

  health_check {
    path = var.hc_path
    protocol = var.service_protocol
    matcher = var.hc_matcher
    interval = var.hc_interval
    timeout = var.hc_timeout
    healthy_threshold = var.hc_treshold_h
    unhealthy_threshold = var.hc_treshold_u
  }
}




#######################################
### LB,  LISTENERS AND LISTENER RULES ###  
#######################################
resource "aws_lb" "webserver-lb" {
  name = var.alb_name
  load_balancer_type = "application"
  subnets = data.aws_subnet_ids.default.ids
  security_groups = [aws_security_group.alb.id]
}

resource "aws_lb_listener" "http" {
  load_balancer_arn = aws_lb.webserver-lb.arn
  port = var.service_port
  protocol = var.service_protocol

  # default action is to return plain 404 page
  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "404: page not found"
      status_code = 404
    }
  }
}

resource "aws_lb_listener_rule" "asg" {
  listener_arn = aws_lb_listener.http.arn
  priority = 100

  condition {
    field = "path-pattern"
    values = ["*"]
  }

  action {
    type = "forward"
    target_group_arn = aws_lb_target_group.asg.arn
  }
}




################################
### AUTOSCALING GROUPS (ASG) ###
################################
resource "aws_autoscaling_group" "webserver-asg" {
  launch_configuration = aws_launch_configuration.webserver-conf.name
  vpc_zone_identifier = data.aws_subnet_ids.default.ids

  target_group_arns = [aws_lb_target_group.asg.arn]
  health_check_type = "ELB"

  min_size = var.asg_min_size
  max_size = var.asg_max_size

  tag {
    key = "Name"
    value = "webserver-asg"
    propagate_at_launch = true
  }
}
