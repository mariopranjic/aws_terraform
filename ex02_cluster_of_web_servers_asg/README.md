# AWS web service cluster example

Based on Terraform up&running book.
In this example we deploy a cluster of web servers and a load balancer in AWS with terraform.

Important links:

* [EC2](https://aws.amazon.com/ec2/)

* [Auto Scaling](https://aws.amazon.com/autoscaling/)

* [ELB](https://aws.amazon.com/elasticloadbalancing/)


The load balancer listens on port 80 and returns simple text when queried.

Compared to original code, variables are added in variables.tf and provide parameter tuning option
at single place.

## Pre-requisites

* You must have [Terraform](https://www.terraform.io/) installed on your computer. 
* You must have an [Amazon Web Services (AWS) account](http://aws.amazon.com/).

Please note that this code was written for Terraform 0.12.x.


## Prerequisites

Since we are using shared location for terraform state (S3 bucket and DynamoDB), make sure you ran ex03 first!

## Quick start


Configure your [AWS access 
keys](http://docs.aws.amazon.com/general/latest/gr/aws-sec-cred-types.html#access-keys-and-secret-access-keys) as 
environment variables:

```
export AWS_ACCESS_KEY_ID=(your access key id)
export AWS_SECRET_ACCESS_KEY=(your secret access key)
```

Deploy the code:

```
terraform init -backend-config=../backend.hcl
terraform apply
```

When the `apply` command completes, it will output the DNS name of the load balancer. To test the load balancer:

```
curl http://<alb_dns_name>/
```

Clean up when you're done:

```
terraform destroy
```
